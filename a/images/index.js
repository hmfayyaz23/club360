export default Images = {
    icons: {
        Me: require('./MeIcon.png'),
        MeFocused: require('./MeFocusIcon.png'),
        MyCoupons: require('./CouponIcon.png'),
        MyCouponsFocused: require('./CouponFocusIcon.png'),
        Promotions: require('./PromoIcon.png'),
        PromotionsFocused: require('./PromoFocusIcon.png'),
        Restaurants: require('./RestaurIcon.png'),
        RestaurantsFocused: require('./RestaurFocusIcon.png'),
        Experiences: require('./ExperIcon.png'),
        ExperiencesFocused: require('./ExperFocusIcon.png'),
    }
};
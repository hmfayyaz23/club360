import React, {useEffect} from 'react';
import {
    View,
    SafeAreaView,
    StyleSheet,
    Dimensions,
    StatusBar,
    ScrollView,
    Image,
    Pressable, Keyboard,
} from 'react-native';
import {Button} from 'react-native-paper';
import BackgroundLayout from '../components/BackgroundLayout';
import LogoBar from '../components/LogoBar';
import TitleBar from '../components/TitleBar';
import Tile from '../components/CategoryTile';

const screenWidth = Dimensions.get('window').width;

const Others = (props) => {

    const [SView, setSView] = React.useState('50%');

    useEffect(() => {
        Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
        Keyboard.addListener("keyboardDidHide", _keyboardDidHide);

        // cleanup function
        return () => {
            Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
            Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
        };
    }, []);

    const _keyboardDidShow = () => {
        setSView('28%');
    };

    const _keyboardDidHide = () => {
        setSView('50%');
    };

    return(
        <SafeAreaView style={styles.container}>
            <BackgroundLayout />
            <LogoBar/>
            <TitleBar
                title={`OTHERS`}
            />
            <View style={{height: SView, marginTop: 20, paddingLeft: '5.55%', borderWidth: 1, borderColor: '#f00'}}>
                <ScrollView showsVerticalScrollIndicator={false}>

                </ScrollView>
            </View>
        </SafeAreaView>
    );
};

export default Others;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

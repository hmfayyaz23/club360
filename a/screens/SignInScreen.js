import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Image,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import {Button, Title, Text} from 'react-native-paper';
import {connect} from 'react-redux';
import {signIn} from '../actions';
import axios from 'axios';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const statusBar = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const SignInScreen = (props) => {
  const [value, onChangeText] = React.useState('');
  const [value2, onChangeText2] = React.useState('');

  const handleInput = (data) => props.signIn(data);

  const authenticate = (email, password) => {
    axios
      .post('http://staging.ask-me-2.com.au/api/v3/login', {
        email: email,
        password: password,
      })
      .then(function (response) {
        //console.log(response);
        handleInput(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  return (
    <SafeAreaView
      style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: '#fff',
      }}>
      <Image
        source={require('../images/background.png')}
        style={styles.background_image}
      />

      <ScrollView style={{flex: 1}} showsVerticalScrollIndicator={false}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            minHeight: 500,
            zIndex: 2,

            width: '100%',
            marginTop: 50,
            //marginTop: -80,
          }}>
          <Image
            source={require('../images/logo.png')}
            style={{resizeMode: 'contain', width: '30%'}}
          />
          <Title style={styles.heading}>Welcome Back! Sign In</Title>
          <TextInput
            style={styles.input}
            onChangeText={(text) => onChangeText(text)}
            value={value}
            placeholder="EMAIL ADDRESS"
            placeholderTextColor="#838e9d"
          />
          <TextInput
            style={styles.input}
            onChangeText={(text) => onChangeText2(text)}
            value={value2}
            placeholder="PASSWORD"
            placeholderTextColor="#838e9d"
          />
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              marginTop: 10,
              marginBottom: 15,
            }}>
            <Text style={{fontSize: 12}}>Forgetten your </Text>
            <TouchableOpacity>
              <Text
                style={{
                  color: '#9e1b95',
                  textDecorationLine: 'underline',
                  fontSize: 12,
                }}>
                Username or Password
              </Text>
            </TouchableOpacity>
          </View>
          <Button
            style={styles.btn}
            color="#fff"
            contentStyle={{}}
            onPress={() => {
              authenticate(value, value2);
            }}>
            SIGN IN
          </Button>

          <Text style={{marginTop: 8, marginBottom: 3, fontSize: 12}}>
            Don’t Have an Account?
          </Text>
          <TouchableOpacity
            onPress={() => props.navigation.navigate('SignUpScreen')}>
            <Text
              style={{
                color: '#9e1b95',
                textDecorationLine: 'underline',
                fontSize: 12,
              }}>
              Sign Up
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => ({isLogged: state.LoginDetails.isLogged, token: state.LoginDetails.token});

const mapDispatchToProps = (dispatch) => ({
  signIn: (data) => {
    dispatch(signIn(data));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen);

const styles = StyleSheet.create({
  btn: {
    backgroundColor: '#e57c0b',
    color: '#fff',
    borderRadius: 10,
    width: '42%',
    height: '11%',
    justifyContent: 'center',
    alignItems: 'center',
  },

  heading: {
    color: '#333333',
    fontSize: 24,
  },
  input: {
    width: '90%',
    height: '11%',
    borderColor: '#f0f3f7',
    borderWidth: 1,
    textAlign: 'center',
    marginBottom: 6,
    marginTop: 6,
    backgroundColor: '#f0f3f7',
    fontWeight: 'bold',
    borderRadius: 8,
    fontSize: 13,
    color: '#838e9d',
  },
  image: {
    flex: 1,
    resizeMode: 'contain',
    justifyContent: 'center',
    borderColor: 'red',
  },
  background_image: {
    height: screenHeight - statusBar,
    width: screenWidth,
    resizeMode: 'stretch',
    position: 'absolute',
    bottom: 0,
  },
});

import React, {useEffect} from 'react';
import {
  View,
  Image,
  Dimensions,
  ImageBackground,
  StatusBar,
  StyleSheet,
  Platform,
  TextInput,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  Keyboard,
} from 'react-native';
import {Button, Title, Text} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon5 from 'react-native-vector-icons/FontAwesome5';
import {signIn} from '../actions';
import {connect} from 'react-redux';
import axios from 'axios';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const statusBar = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const Me = (props) => {
  const [SView, setSView] = React.useState('69.3%');
    //const [categories, setCategories] = React.useState([]);

  useEffect(() => {


    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', _keyboardDidHide);
    };
  }, []);

  const _keyboardDidShow = () => {
    setSView('50%');
  };

  const _keyboardDidHide = () => {
    setSView('69.3%');
  };

  const test = (data) => {
      console.log(Object.values(data).length, 'length');
      return (Object.values(data).length) !== 0 ?
      Object.values(data).slice(0,2) : ''
  };
  return (
    <SafeAreaView style={styles.container}>
      <Image
        source={require('../images/background2.png')}
        style={styles.background_image}
      />
      <View style={styles.logo_row}>
        <View style={styles.logo_row_col_1}>
          <Image
            source={require('../images/homeLogo.png')}
            style={styles.logo_css}
          />
        </View>
        <View style={styles.logo_row_col_2}>
          <View style={styles.logo_row_col_2_view_1}>
            <Text
              style={{
                fontWeight: '700',
                color: '#5a5a5a',
                textAlignVertical: 'center',
              }}>
              AREA SELECTED
            </Text>
            <Icon
              name="map-marker"
              size={24}
              color="#5a5a5a"
              style={{marginLeft: 7}}
            />
          </View>
          <View style={styles.logo_row_col_2_view_2}>
            <View
              style={{
                borderWidth: 1,
                borderRadius: 15,
                paddingHorizontal: 6,
                flexDirection: 'row',
                flex: 1,
                alignContent: 'center',
                justifyContent: 'center',
                borderColor: '#cac8c8',
              }}>
              <TextInput
                placeholder="SEARCH FOR A BUSINESS"
                style={{
                  textDecorationLine: 'none',
                  fontWeight: 'bold',
                  fontSize: 11,
                  overflow: 'hidden',
                  height: 33,
                  justifyContent: 'center',
                  //textAlign: 'center',
                  width: '90%',
                  letterSpacing: 0.5,
                }}
                placeholderTextColor="#333333"
              />
              <Icon5
                name="search"
                size={15}
                color="#5a5a5a"
                style={{marginTop: 6, marginRight: 7}}
              />
            </View>
            <View
              style={{
                borderWidth: 1,
                padding: 6,
                borderRadius: 32,
                overflow: 'hidden',
                marginLeft: 5,
                borderColor: '#cac8c8',
              }}>
              <Icon name="bell" size={18} color="#5a5a5a" />
            </View>
          </View>
        </View>
      </View>
      <View style={{marginTop: 20, marginHorizontal: '5.55%', height: SView}}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.contentScroll_view_1}>
              <View style={styles.contentScroll_view_1_row}>
                <Button
                  style={{
                    backgroundColor: '#e57c0b',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 14,
                  }}
                  color="#fff"
                  labelStyle={{fontSize: 9, textAlign: 'center'}}>
                  LOGOUT
                </Button>
                <Button
                  style={{
                    backgroundColor: '#c219b7',
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderRadius: 14,
                    marginLeft: '3.11%',
                  }}
                  color="#fff"
                  labelStyle={{fontSize: 9, textAlign: 'center'}}>
                  HELP
                </Button>
              </View>
              <TouchableOpacity style={styles.touchableBox_left}>
                <Image
                  source={require('../images/couponCoin.png')}
                  style={{height: '38.31%', resizeMode: 'contain'}}
                />
                <Title
                  style={{
                    fontSize: 14,
                    lineHeight: 14,
                    textAlign: 'center',
                    marginTop: '8.42%',
                  }}>
                  MY 360 PASS{'\n'}& COUPON
                </Title>
              </TouchableOpacity>
              <TouchableOpacity style={styles.touchableBox_left}>
                <Image
                  source={require('../images/markerCoin.png')}
                  style={{height: '38.31%', resizeMode: 'contain'}}
                />
                <Title
                  style={{
                    fontSize: 14,
                    lineHeight: 14,
                    textAlign: 'center',
                    marginTop: '8.42%',
                  }}>
                  WHAT'S ON
                </Title>
              </TouchableOpacity>
              <TouchableOpacity style={styles.touchableBox_left}>
                <Image
                  source={require('../images/promoCoin.png')}
                  style={{height: '38.31%', resizeMode: 'contain'}}
                />
                <Title
                  style={{
                    fontSize: 14,
                    lineHeight: 14,
                    textAlign: 'center',
                    marginTop: '8.42%',
                  }}>
                  SPECIAL PROMOTION{'\n'}& OFFERS
                </Title>
              </TouchableOpacity>
            </View>
            <View style={styles.contentScroll_view_2}>
              <Title
                style={{
                  fontSize: 13,
                  lineHeight: 14,
                  textAlign: 'center',
                  marginTop: '5%',
                }}>
                360 PASS{'\n'}PARTNERS
              </Title>
                {
                    !props.loader ?
                        <TouchableOpacity
                            style={styles.touchableBox_right}
                            onPress={() => props.navigation.navigate('Restaurants')}>
                            <Image
                                source={require('../images/restaurantCoin.png')}
                                style={{height: '38.31%', resizeMode: 'contain'}}
                            />
                            <Title
                                style={{
                                    fontSize: 14,
                                    lineHeight: 14,
                                    textAlign: 'center',
                                    marginTop: '8.42%',
                                }}>
                                Wait
                            </Title>
                        </TouchableOpacity>
                        :
                        test(props.categories).map((value,key) => (
                            <TouchableOpacity
                                style={styles.touchableBox_right}
                                onPress={() => props.navigation.navigate('Restaurants')}>
                                <Image
                                    source={require('../images/restaurantCoin.png')}
                                    style={{height: '38.31%', resizeMode: 'contain'}}
                                />
                                <Title
                                    style={{
                                        fontSize: 14,
                                        lineHeight: 14,
                                        textAlign: 'center',
                                        marginTop: '8.42%',
                                    }}>
                                    {value.name}
                                </Title>
                            </TouchableOpacity>
                        ))
                }

              <TouchableOpacity style={styles.touchableBox_right_last} onPress={() => props.navigation.navigate('Others')}>
                <Image
                  source={require('../images/otherCoin.png')}
                  style={{height: '38.31%', resizeMode: 'contain'}}
                />
                <Title
                  style={{
                    fontSize: 14,
                    lineHeight: 14,
                    textAlign: 'center',
                    marginTop: '8.42%',
                  }}>
                  OTHER
                </Title>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const mapStateToProps = (state) => ({
  isLogged: state.LoginDetails.isLogged,
  categories: state.Categories,
    loader: state.Loader,
});

const mapDispatchToProps = (dispatch) => ({
  signIn: (data) => {
    dispatch(signIn(data));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Me);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },

  logo_row: {
    flexDirection: 'row',
  },

  logo_row_col_1: {
    width: '35%',
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },

  logo_row_col_2: {
    marginTop: 20,
    width: '65%',
  },

  logo_row_col_2_view_1: {
    alignSelf: 'flex-end',
    justifyContent: 'center',
    flexDirection: 'row',
    marginRight: '5.55%',
  },

  logo_row_col_2_view_2: {
    alignSelf: 'flex-end',
    justifyContent: 'center',
    flexDirection: 'row',
    marginRight: '5.55%',
    marginTop: 12,
    height: 33,
  },

  logo_css: {
    width: screenWidth * 0.2555,
    height: screenHeight * 0.12,
    resizeMode: 'contain',
  },

  background_image: {
    height: screenHeight - statusBar,
    width: screenWidth,
    resizeMode: 'stretch',
    position: 'absolute',
    bottom: 0,
  },

  contentScrollView: {
    flex: 1,
    marginTop: 20,
    width: screenWidth,
    maxHeight: '69.3%',
    paddingLeft: '5.55%',
    paddingRight: '5.55%',
    //   borderWidth: 1,
    //   borderColor: '#f00'
  },

  contentScroll_view_1: {
    flex: 1,
    justifyContent: 'center',
    width: '50%',
    paddingRight: '4.67%',
    alignContent: 'flex-start',
    maxHeight: '100%',
  },

  contentScroll_view_1_row: {
    flex: 1,
    flexDirection: 'row',
    maxHeight: 30,
  },

  contentScroll_view_2: {
    justifyContent: 'center',
    width: '50%',
    height: '100%',
    backgroundColor: '#ced5e0',
    paddingLeft: '2%',
    paddingRight: '2%',
    borderRadius: 10,
  },

  touchableBox_left: {
    borderWidth: 1,
    borderColor: '#cac8c8',
    borderRadius: 10,
    height: 145,
    backgroundColor: '#fff',
    marginTop: '7.5%',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },

  touchableBox_right: {
    borderWidth: 1,
    borderColor: '#cac8c8',
    borderRadius: 10,
    height: '29.4%',
    backgroundColor: '#fff',
    marginTop: '5%',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  touchableBox_right_last: {
    borderWidth: 1,
    borderColor: '#cac8c8',
    borderRadius: 10,
    height: '27%',
    backgroundColor: '#fff',
    marginTop: '5%',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
});

export const signIn = (data) => {
    return {
        type: 'SIGN_IN',
        payload: data
    };
};
export const categories = (data) => {
    return {
        type: 'ADD_CATEGORIES',
        payload: data
    };
};
export const loader = (data) => {
    return {
        type: 'LOADER',
        payload: data
    };
};

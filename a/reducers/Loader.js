const loader = (state = false, action) => {
    switch ( action.type ){
        case 'LOADER':
            let response = action.payload;
            return response;

        default:
            return state;
    }
};

export default loader;
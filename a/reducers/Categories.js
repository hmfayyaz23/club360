const initialState = [];

const categories = (state = initialState, action) => {
    switch ( action.type ){
        case 'ADD_CATEGORIES':
            let response = action.payload;
            if(response){
                //console.log(response , "red");
                return response;
            }else {
                return state;
            }

        default:
            return state;
    }
};

export default categories;
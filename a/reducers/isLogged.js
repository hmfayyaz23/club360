let initialState = {
    isLogged: false,
    token: ''
};

const loggedReducer = (state = initialState, action) => {
    switch ( action.type ){
        case 'SIGN_IN':
            let response = action.payload.data.status;
            let gtoken = action.payload.data.data.token;
            console.log(gtoken, 'reducer');
            if (response){
                console.log('Logged In');
                return { ...state, isLogged: true, token: gtoken};
            }else{
                console.log('Log In Failed');
                return state;
            }


        default:
            return state;
    }
};

export default loggedReducer;
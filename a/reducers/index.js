//import counterReducer from './counter';
import loggedReducer from './isLogged';
import categories from './Categories';
import loader from './Loader';

import {combineReducers} from 'redux';

const allReducers = combineReducers({
    LoginDetails: loggedReducer,
    Categories: categories,
    Loader: loader,
});

export default allReducers;
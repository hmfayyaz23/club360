import React from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import Btn from '../components/Btn';

const TitleBar = (props) => {
  return (
    <View style={{flexDirection: 'row', paddingHorizontal: '5.55%', marginTop: 18}}>
        <Text style={{fontSize: 20, fontWeight: '700'}}>{props.title}</Text>
        <View style={{flexDirection: 'row',height: 30,flex: 1, justifyContent: 'flex-end'}}>
          <Btn label='Home' style={{backgroundColor: '#c219b7'}}/>
          <Btn label='Back' style={{backgroundColor: '#e57c0b' , marginLeft: 5}}/>
        </View>
      </View>
  );
};

export default TitleBar;

const styles = StyleSheet.create({
  
});

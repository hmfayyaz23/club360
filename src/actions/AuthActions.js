import {EMAIL_CHANGED, PASSWORD_CHANGED, LOGIN_USER_SUCCESS, LOGIN_USER_FAIL} from './types';
import axios from 'axios';
import {apiActiveURL} from '../ApiBaseURL';

export const emailChanged = (text) => {
  return {
    type: EMAIL_CHANGED,
    payload: text,
  };
};

export const passwordChanged = (text) => {
  return {
    type: PASSWORD_CHANGED,
    payload: text,
  };
};

export const loginUser = ({email, password}) => {
  return (dispatch) => {
    const url = `${apiActiveURL}/login`;
    axios
      .post(url, {
        email: email,
        password: password,
      })
      .then((res) => loginUserSuccess(dispatch, res))
      .catch(() => loginUserFail(dispatch))
  };
};

const loginUserSuccess = (dispatch, res) => {
    dispatch({type: LOGIN_USER_SUCCESS, payload: res});
};

const loginUserFail = (dispatch) => {
    dispatch({type: LOGIN_USER_FAIL});
};
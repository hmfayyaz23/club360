import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image,
    TextInput,
    TouchableOpacity,
    ScrollView,
    Dimensions,
    SafeAreaView,
    StatusBar,
} from 'react-native';
import { Button, Title, Text } from 'react-native-paper';
import { connect } from 'react-redux';
import { emailChanged, passwordChanged, loginUser } from '../actions';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const statusBar = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    onEmailChange = (text) => {
        this.props.emailChanged(text);
    }

    onPasswordChange = (text) => {
        this.props.passwordChanged(text);
    }

    handleLogin = () => {
        const {email, password} = this.props;
        this.props.loginUser({email, password});
    }

    render() {
        console.log(this.props.user);
        return (
            <SafeAreaView
                style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                    backgroundColor: '#fff',
                }}>
                <Image
                    source={require('../assets/images/background.png')}
                    style={styles.background_image}
                />

                <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
                    <View
                        style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                            minHeight: 500,
                            zIndex: 2,
                            width: '100%',
                            marginTop: 50,
                        }}>
                        <Image
                            source={require('../assets/images/logo.png')}
                            style={{ resizeMode: 'contain', width: '30%' }}
                        />
                        <Title style={styles.heading}>Welcome Back! Sign In</Title>
                        <TextInput
                            style={styles.input}
                            value={this.props.email}
                            onChangeText={this.onEmailChange}
                            placeholder="EMAIL ADDRESS"
                            placeholderTextColor="#838e9d"
                        />
                        <TextInput
                            style={styles.input}
                            value={this.props.password}
                            onChangeText={this.onPasswordChange}
                            secureTextEntry
                            placeholder="PASSWORD"
                            placeholderTextColor="#838e9d"
                        />
                        <View
                            style={{
                                display: 'flex',
                                flexDirection: 'row',
                                marginTop: 10,
                                marginBottom: 15,
                            }}>
                            <Text style={{ fontSize: 12 }}>Forgetten your </Text>
                            <TouchableOpacity>
                                <Text
                                    style={{
                                        color: '#9e1b95',
                                        textDecorationLine: 'underline',
                                        fontSize: 12,
                                    }}>
                                    Username or Password
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <Button
                            onPress={this.handleLogin}
                            style={styles.btn}
                            color="#fff"
                        >
                            SIGN IN
                        </Button>

                        <Text style={{ marginTop: 8, marginBottom: 3, fontSize: 12 }}>
                            Don’t Have an Account?
                        </Text>
                        <TouchableOpacity>
                            <Text
                                style={{
                                    color: '#9e1b95',
                                    textDecorationLine: 'underline',
                                    fontSize: 12,
                                }}>
                                Sign Up
                            </Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const mapStateToProps = state => {
    return {
        email: state.auth.email,
        password: state.auth.password,
        error: state.auth.error,
        user: state.auth.user
    };
};

export default connect(mapStateToProps, { emailChanged, passwordChanged, loginUser })(LoginForm);

const styles = StyleSheet.create({
    btn: {
        backgroundColor: '#e57c0b',
        color: '#fff',
        borderRadius: 10,
        width: '42%',
        height: '11%',
        justifyContent: 'center',
        alignItems: 'center',
    },

    heading: {
        color: '#333333',
        fontSize: 24,
    },
    input: {
        width: '90%',
        height: '11%',
        borderColor: '#f0f3f7',
        borderWidth: 1,
        textAlign: 'center',
        marginBottom: 6,
        marginTop: 6,
        backgroundColor: '#f0f3f7',
        fontWeight: 'bold',
        borderRadius: 8,
        fontSize: 13,
        color: '#838e9d',
    },
    image: {
        flex: 1,
        resizeMode: 'contain',
        justifyContent: 'center',
        borderColor: 'red',
    },
    background_image: {
        height: screenHeight - statusBar,
        width: screenWidth,
        resizeMode: 'stretch',
        position: 'absolute',
        bottom: 0,
    },
});